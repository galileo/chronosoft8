#!/bin/bash
# Il n'y a pas de boutton "HOME" sur cette télécommande
# le seul moyen d'aller sur le canal n°1 est de désactiver puis réactiver tous les cannaux

## Configuration
CANNAUX_MAX=8
IP="192.168.1.44"

## Commandes de DOWNe
PUSH_SHORT=200
PUSH_LONG=3500

## boutton
LEFT=4
RIGHT=5
DOWN=12
STOP=13
UP=14
ONOFF=15

for i in {1..8}
do
	curl http://$IP/control?cmd=Pulse,$LEFT,1,$PUSH_SHORT
	sleep 1
done

#mode config
curl http://$IP/control?cmd=Pulse,$RIGHT,1,$PUSH_LONG
sleep 1

#le canal1 est validé
curl http://$IP/control?cmd=Pulse,$RIGHT,1,$PUSH_SHORT
sleep 1

for i in {2..8}
do
#les canaux 2 à 8 sont désactivés
	curl http://$IP/control?cmd=Pulse,$DOWN,1,$PUSH_SHORT
	sleep 1
	curl http://$IP/control?cmd=Pulse,$RIGHT,1,$PUSH_SHORT
	sleep 1
done

#mode config
curl http://$IP/control?cmd=Pulse,$RIGHT,1,$PUSH_LONG
sleep 1

for i in {1..8}
do
#tous les canaux sont réactivés
	curl http://$IP/control?cmd=Pulse,$UP,1,$PUSH_SHORT
	sleep 1
	curl http://$IP/control?cmd=Pulse,$RIGHT,1,$PUSH_SHORT
	sleep 1
done
