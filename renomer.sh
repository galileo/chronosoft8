#!/bin/bash
# le script base.sh provoque la perte de tous les noms de cannaux
# ce script permet de les renomer

echo Reveille avec le stop
curl http://192.168.1.44/control?cmd=Pulse,13,1,200
sleep 1

#  0 - ENTRE 
#  1 - CUISINE
#  2 - SALON
#  3 - SALLE A MANGER
#  4 - BUREAU 
#  5 - CHAMBRE
#  6 - SALLE DE JEUX
#  7 - TOILETTES
#  8 - SALLE DE BAIN
#  9 - RDC
# 10 - ETAGE
# 11 - GARAGE
# 12 - GENERALE
# 13 - AUTRES


declare -a array=( "12" "4" "3" "2" "8" "10" "5" "1" )

arraylength=${#array[@]}

for (( i=1; i<${arraylength}+1; i++ ))

do
	echo "#############################"
	val=${array[$i-1]}
	echo $i - $val
    
    echo Bouton menu
	curl http://192.168.1.44/control?cmd=Pulse,5,1,200
	sleep 1
	
	echo choix "nommer canal"
	curl http://192.168.1.44/control?cmd=Pulse,12,1,200
	sleep 1
	curl http://192.168.1.44/control?cmd=Pulse,12,1,200
	sleep 1
	
	echo Bouton valider
	curl http://192.168.1.44/control?cmd=Pulse,5,1,200
	sleep 1
	
	for j in $(seq 1 $val)
	do
		echo "**********************"
		echo $i - $j
		echo Bouton bas
		curl http://192.168.1.44/control?cmd=Pulse,12,1,200
		sleep 1
	done
	
	echo Bouton valider
	#on valide 2 fois
	curl http://192.168.1.44/control?cmd=Pulse,5,1,200
	sleep 1
	curl http://192.168.1.44/control?cmd=Pulse,5,1,200
	sleep 1
	
	echo  Bouton canal, on change de canal
	curl http://192.168.1.44/control?cmd=Pulse,4,1,200
	sleep 1
		
done
