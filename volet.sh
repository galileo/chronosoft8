#!/bin/bash
# Commande des volets en bash

IP="192.168.1.44"

## Commandes de DOWNe
PUSH_SHORT=200
PUSH_LONG=3500


## boutton
LEFT=4
RIGHT=5
DOWN=12
STOP=13
UP=14
ONOFF=15



case "$1" in
        return)
			curl http://$IP/control?cmd=GPIO,$LEFT,1
			curl http://$IP/control?cmd=GPIO,$LEFT,0
            ;;
            
        valid)
			curl http://$IP/control?cmd=GPIO,$RIGHT,1
			curl http://$IP/control?cmd=GPIO,$RIGHT,0
            ;;
        
        up)
			curl http://$IP/control?cmd=GPIO,$UP,1
			curl http://$IP/control?cmd=GPIO,$UP,0
            ;;
         
        stop)
			curl http://$IP/control?cmd=GPIO,$STOP,1
			curl http://$IP/control?cmd=GPIO,$STOP,0
            ;;
         
        down)
			# curl http://$IP/control?cmd=GPIO,$DOWN,2
			# curl http://$IP/control?cmd=GPIO,$DOWN,0
			curl http://$IP/control?cmd=Pulse,$DOWN,1,$PUSH_SHORT
            ;;
         
        *)
            echo $"Usage: $0 {return|valid|up|stop|down}"
            exit 1
 
esac
